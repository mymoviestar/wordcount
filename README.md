# My project's README

Objective: parshing string

Compile $g++ wordcount.cpp

Run $./a.out

A.out and alidin.txt files should be in the same directory

If you need to change other files, change the "aliding.txt" file to the desired file in the wordcount.cpp file.

Assignment1
#==================================================================================================
Create an account at bitbucket.

Download the following text script.
http://www.fpx.de/fp/Disney/Scripts/Aladdin.txt

Write a C++ code which does the followings:
Read a text file as an argument: 
assign1 Aladdin.txt
Tokenize the text file such that
Divide a sentence into words
Each word does not include following symbols:
\t (tab), \r, \n, ' ' (space), '"', ':', ',', ';', ‘’’

It is allowed to have ‘’’ in the middle of a word.
Change all upper-case letters to lower-case ones.
Count the number of occurrence of each distinguished word
Sort the word by their frequency (# of occurrence) in descending order.
Print (into standard output) the pair of word and its frequency in the following way (one pair per line):
the: 10000
a: 9900
...
#==================================================================================================
