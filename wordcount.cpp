#include <iostream>
#include <string.h>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace std;

struct WORD {
	string str;
	int count;
};

vector <WORD*> words;

int FindWords(const string& s);
void CountWords(const string& s);
void ShowWords();
void RemoveAll();

int main() {

	vector <string> wordVector;
	vector <int>::iterator iter;

    ifstream file("aliddin.txt");

	string line;
	while(getline(file,line)) 
	{
		size_t prev = 0, pos, prev_temp = 0;
		while ((pos = line.find_first_of("	\r\n \":,;'.?!()-{}", prev_temp)) != string::npos)
		{
			if('\'' == line[pos]){
				if(' '==line[pos-1] || ' '==line[pos+1]){
					if (pos > prev)
						CountWords(line.substr(prev, pos-prev));
						prev_temp = pos+1;
						prev = prev_temp;
					}
					else{
						prev_temp = pos+1;
					}
			}
			else{
				if (pos > prev)
					CountWords(line.substr(prev, pos-prev));
				prev_temp = pos+1;
				prev = prev_temp;
			}
		}
		if (prev < line.length())
			CountWords(line.substr(prev, string::npos));	
	}
	ShowWords();
	RemoveAll();
}

void CountWords(const string& s2) {
	string s;
	s = s2;
	transform(s.begin(), s.end(), s.begin(), ::tolower);
	int index = FindWords(s);
	if(index == -1) {
		WORD *pWord = new WORD;
		pWord->str = s;
		pWord->count = 1;
		words.push_back(pWord);
	}
	else {
		words[index]->count++;
	}
}

int FindWords(const string& s) {
	for(int i = 0; i<words.size(); i++){
		if(words[i]->str == s)
			return i;
	}
	return -1;
}

bool comp(WORD* a, WORD* b) {
	return (a->count > b->count);
}

void ShowWords() {
	sort(words.begin(), words.end(), comp);
	for(int i=0; i<words.size(); i++)
		cout << words[i]->str << " : " << words[i]->count << endl;
}

void RemoveAll() {
	for(int i=0; i<words.size(); i++)
		delete words[i];
}

